# patentes

El proyecto consiste en la automatización del sistema de una playa de estacionamiento, mediante la utilización del software OpenALPR, con el cual se reconocerán las patentes de los autos entrantes y serán almacenadas en una base de datos para llevar un recuento del tiempo que permanecen en el establecimiento y así poder calcular su tarifa, además se implementará una pagina web para poder acceder a un log de estos datos.
También, se utilizarán sensores detectores de obstáculos (IR) como detectores de automóviles, en una forma de abstracción, para tener un control sobre los lugares libres y ocupados del estacionamiento; en este proyecto contaremos con dos espacios para estacionar.



