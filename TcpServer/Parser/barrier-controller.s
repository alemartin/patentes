@ ---------------------------------------
@	Data Section
@ ---------------------------------------
	
		.data
		.balign 4	
OnMsg:		.asciz  "Turning GREEN LED on\n"
redMsg:		.asciz	"RED LED on\n"
OffMsg:		.asciz  "Turning LED off\n"
ErrMsg:		.asciz	"Error on WiringPi setup\n"
i:			.int 	0
pin1:		.int	7
pin2:		.int 	0
delay6:		.int 	6000
delay2:		.int 	1000

OUTPUT = 1
	
@ ---------------------------------------
@	Code Section
@ ---------------------------------------
	
	.text
	.global asm_main
	.extern printf
	.extern wiringPiSetup
	.extern delay
	.extern digitalWrite
	.extern pinMode


@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@ main
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	
asm_main:   
@ Push return address + dummy register
	push {ip, lr}

@ Muevo argumento de la llamada, éstos
@	se guardan en los registros r0 a r3
	mov	r5, r0

@ Chequeo si wiringPiSetup devuelve -1
@	de ser así se produjo un error
	bl	wiringPiSetup
	mov	r1, #-1
	cmp	r0, r1
	@ Caso contrario chequeo argumento
	bne	checkArg
	ldr	r0, =ErrMsg
	bl	printf
	b	done


@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@ Chequeo el argumento recibido
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

checkArg:
@ Comparo el parámetro que me envian
@	si es 1 prendo verde, si no rojo	
	cmp r5, #1
	beq	onVerde
	b 	blinkRojo


@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@ Prende un LED por unos segundos
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

onVerde:

@ Seteo al pin como salida
	ldr	r0, =pin1
	ldr	r0, [r0]
	mov	r1, #OUTPUT
	bl	pinMode

@ Imprimo 'OnMsg'
@	ldr	r0, =OnMsg
@   bl 	printf
	
@ Pongo Pin en alto (prendo LED)
	ldr	r0, =pin1
	ldr	r0, [r0]
	mov	r1, #1
	bl 	digitalWrite
	
@ Delay de 2s
	ldr	r0, =delay6
	ldr	r0, [r0]
	bl	delay

@ Imprimo 'OffMsg'
@	ldr	r0, =OffMsg
@   bl 	printf

@ Pongo Pin en bajo (apago LED)
	ldr	r0, =pin1
	ldr	r0, [r0]
	mov	r1, #0
	bl 	digitalWrite

@ Termino
	b done


@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@ Titila un LED 3 veces
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

blinkRojo:

@ Seteo al pin como salida
	ldr	r0, =pin2
	ldr	r0, [r0]
	mov	r1, #OUTPUT
	bl	pinMode

@ Hago un bucle para prender
@	y apagar el LED

@ for(i=0; i<2; i++){
	ldr	r4, =i
	ldr	r4, [r4]
	mov	r5, #2
forLoop:
	cmp	r4, r5
	bgt	done

@ Imprimo 'OnMsg'
@	ldr	r0, =redMsg
@    bl 	printf
	
@ Pongo Pin en alto (prendo LED)
	ldr	r0, =pin2
	ldr	r0, [r0]
	mov	r1, #1
	bl 	digitalWrite
	
@ Delay de 2s
	ldr	r0, =delay2
	ldr	r0, [r0]
	bl	delay

@ Imprimo 'OffMsg'
@	ldr	r0, =OffMsg
@    bl 	printf

@ Pongo Pin en bajo (apago LED)
	ldr	r0, =pin2
	ldr	r0, [r0]
	mov	r1, #0
	bl 	digitalWrite

@ Delay de 2s
	ldr	r0, =delay2
	ldr	r0, [r0]
	bl	delay

@ i++; }
	add	r4, #1
	b	forLoop


@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@ Fin del programa
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@

done:
@ Pop return address into pc	
    pop	{ip, pc}
