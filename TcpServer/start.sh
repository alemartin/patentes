#!/bin/bash

set -m
echo "Make Parser"
cd Parser
make
cd ../Sensor
echo "Make Sensor"
make
cd ../
echo "Iniciando servidor..."
python3 Server.py -i 192.168.0.188 &
echo "Insertando modulo"
sudo insmod Sensor/sensor_d.ko pid=$(pidof python3)
clear
fg
