package com.am.lpcamera;

import android.content.pm.ActivityInfo;
import android.hardware.Camera;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.IOException;
import java.util.Base64;

public class MainActivity extends AppCompatActivity implements SurfaceHolder.Callback {

    static TcpClient cliente;
    Button sendButton;
    SurfaceView sv;
    SurfaceHolder sHolder;
    EditText ipIngress;
    Camera camara;

    private String serverIP;

    private static boolean arranque = true;

    Camera.PictureCallback mCall;

    static Handler handler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        sendButton = (Button) findViewById(R.id.send_btn);
        sv = (SurfaceView) findViewById(R.id.surfaceView);
        ipIngress = (EditText) findViewById(R.id.ip_et);

        if(arranque){
            sendButton.setVisibility(View.VISIBLE);
            ipIngress.setVisibility(View.VISIBLE);
        }

        new ConnectTask().execute("");

        handler = new Handler() {
            @Override
            public void handleMessage(android.os.Message msg) {
                if (msg.what == TcpClient.MSGOK) {
                    //modifico la pantalla para tomar foto
                    setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
                }
                if(msg.what == TcpClient.MSGWT){
                    //espero al sv
                    cliente.sendMessage("WAITING");
                }
            }
        };

        sHolder = sv.getHolder();
        sHolder.addCallback(this);
        sHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);

        sendButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                serverIP = ipIngress.getText().toString();
                Log.d("TCP", "SV IP: "+serverIP);

                sendButton.setVisibility(View.GONE);
                ipIngress.setVisibility(View.GONE);

                try{
                    Thread.sleep(500);
                } catch (Exception e){
                    e.printStackTrace();
                }

                new ConnectTask().execute("");

                try{
                    Thread.sleep(1000);
                } catch (Exception e){
                    e.printStackTrace();
                }

                if (cliente != null) {
                    arranque = false;
                    cliente.sendMessage("WAITING");
                }
                else {
                    Log.d("TCP", "THERE IS NO CONNECTION");
                    sendButton.setVisibility(View.VISIBLE);
                    ipIngress.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        camara = Camera.open();
        try {
            camara.setPreviewDisplay(surfaceHolder);

        } catch (IOException exception) {
            camara.release();
            camara = null;
        }

    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {

        /* Seteo calidad baja (10/100) de Jpeg */
        Camera.Parameters params = camara.getParameters();
        params.setJpegQuality(10);
        camara.setParameters(params);
        camara.startPreview();

        mCall = new Camera.PictureCallback()
        {
            @Override
            public void onPictureTaken(byte[] data, Camera camera)
            {
                // codifico la foto a base64
                @SuppressWarnings("Since15") String message = Base64.getEncoder().encodeToString(data);

                /* Envio la foto */
                if (cliente != null) {
                    Log.d("TCP SEND", "ENVIO FOTO - "+Thread.currentThread().getName());
                    cliente.sendMessage(message);
                }
                else{
                    Log.d("TCP SEND", "TCPCLIENT = NULL - "+Thread.currentThread().getName());
                }
            }
        };

        /* Vuelvo la orientación de la pantalla al estado original */
        if(getRequestedOrientation() == ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE){
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        }
        else{
            camara.takePicture(null, null, mCall);
        }
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        camara.stopPreview();
        camara.release();
        camara = null;
    }


    private class ConnectTask extends AsyncTask<String, String, TcpClient> {

        /* No se puede realizar una 'networking operation'
            en el main thread, necesito de una AsyncTask
         */

        @Override
        protected TcpClient doInBackground(String... message) throws RuntimeException{

            // Creo objeto cliente
            cliente = new TcpClient(serverIP);

            /* Trato de correrlo con el ServerIP entregado
                Capturo la excepción de no ser posible y dejo a cliente como null
            */
            try{
                cliente.run();
            } catch (IOException e){
                Log.d("TCP", "NO SE PUDO CREAR CLIENTE"+Thread.currentThread().getName());
                cliente = null;
            }

            return null;
        }

        /*@Override
        protected void onProgressUpdate(String... values) {}*/
    }
}