package com.am.lpcamera;

import android.os.Message;
import android.util.Log;
import android.widget.EditText;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.InetAddress;
import java.net.Socket;
import java.net.UnknownHostException;

public class TcpClient {

    public static final String TAG = TcpClient.class.getSimpleName();
    //public static final String SERVER_IP = "192.168.0.133"; //direccion IP del servidor
    public static String SERVER_IP; //direccion IP del servidor
    public static final int SERVER_PORT = 27418; //numero de puerto TCP

    static final int MSGOK = 111;
    static final int MSGWT = 113;

    // socket tcp
    private Socket socket;
    // while this is true, the server will continue running
    private boolean mRun = false;
    // buffer de envío
    private PrintWriter sendBuff;
    // se usa para recibir los mensajes
    private BufferedReader readBuff;

    TcpClient(String ip){
        SERVER_IP = ip;
    }


    /*Envia un mensaje al servidor,
    distingue si envia "WAITING" -> se bloquea
    esperando respuesta del servidor para enviar
    la imagen...
     */
    public void sendMessage(final String message) {

        Runnable runnable = new Runnable() {
            @Override
            public void run() {

                /* Al iniciarse la aplicación triggerea
                    un envio de foto indeseado, lo ignoramos */

                if (sendBuff != null) {

                    /* Creo buffer de lectura */
                    try {
                        readBuff = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    }
                    catch (Exception e){
                        e.printStackTrace();
                    }


                    String response = "";

                    /* si el mensaje a enviar es = a WAITING espero en un bucle
                        hasta que el servidor me haga un pedido de foto */
                    if(message.equals("WAITING")){

                        sendBuff.println(message);
                        sendBuff.flush();

                        Log.d(TAG, "WAITING - "+Thread.currentThread().getName());

                        while (true){

                            try {
                                // lee una linea (tiene que terminar con \n)
                                response = readBuff.readLine();
                            } catch (Exception e){
                                e.printStackTrace();
                            }

                            if(response.equals("OK")){
                                Message msg = new Message();
                                msg.what = MSGOK;
                                MainActivity.handler.sendMessage(msg);
                                return;
                            }
                            else {
                                sendBuff.println(message);
                                sendBuff.flush();
                            }
                        }
                    }
                    /* Si no, envío la imagen */
                    else{

                        // envio tamaño del mensaje
                        sendBuff.println(message.length());
                        sendBuff.flush();

                        /* espero confirmación de recepción del tamaño */
                        try {
                            response = readBuff.readLine();
                        } catch (Exception e){
                            e.printStackTrace();
                        }

                        if(response.equals("OKK")) {

                            Log.d(TAG, "ENVIO FOTO - "+Thread.currentThread().getName());

                            /* mando foto */
                            sendBuff.println(message);
                            sendBuff.flush();

                            /* espero confirmación de recepción de imagen */
                            try {
                                response = readBuff.readLine();
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            /* thread espera por nuevo pedido */
                            Message msg = new Message();
                            msg.what = MSGWT;
                            MainActivity.handler.sendMessage(msg);
                        }
                    }
                }
                else{
                    Log.d(TAG, "SEND BUFF NULL - "+Thread.currentThread().getName());
                }
            }
        };

        Thread thread = new Thread(runnable);
        thread.start();
    }

    /**
     * Close the connection and release the members
     */
    public void stopClient() {

        mRun = false;

        if (sendBuff != null) {
            sendBuff.flush();
            sendBuff.close();
        }

        sendBuff = null;
    }

    public void run() throws IOException{

        mRun = true;

        //here you must put your computer's IP address.
        InetAddress serverAddr = InetAddress.getByName(SERVER_IP);

        Log.d("TCP Client", "C: Connecting...");

        //create a socket to make the connection with the server
        socket = new Socket(serverAddr, SERVER_PORT);

        Log.d("TCP Client", "C: Socket Creado");

        try {

            //sends the message to the server
            //sendBuff = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), true);
            sendBuff = new PrintWriter(new BufferedWriter(new OutputStreamWriter(socket.getOutputStream())), false);

            Log.d("TCP Client", "C: Buffer Creado");

            //in this while the client listens for the messages sent by the server
            while (mRun) {

            }

        } catch (Exception e) {
            Log.e("TCP", "S: Error", e);
        } finally {
            //the socket must be closed. It is not possible to reconnect to this socket
            // after it is closed, which means a new socket instance has to be created.
            try{
                socket.close();
            } catch (IOException e){
                Log.e("TCP", "Closing Socket: Error", e);
            }
        }

    }

}