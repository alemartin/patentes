#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "cdecl.h"

#define SIZE 3500
#define OUT "parsed.txt"

int PRE_CDECL asm_main(int true) POST_CDECL;

int main(int argc, char *argv[]) {
	
	/* Compruebo que se haya pasado 1 argumento */
	if(argc != 2){
		printf("Wrong Use!\n");
		return 3;
	}

	char buff[SIZE];

	FILE *fp;
	fp = fopen(argv[1], "r");
	
	/* Compruebo que el archivo existe */
	if(fp == NULL){
		printf("Invalid file name!\n");
		return 1;
	}

	fread(buff, SIZE, 1, fp);
	fclose(fp);

	/* Compruebo que el programa haya encontrado
		una patente */
	if(!strcmp(buff, "No license plates found.\n")){
		//printf("No license plate found!\n");
		asm_main(0);
		return 2;
	}

	/* Me muevo hasta el comienzo de la patente,
		e ignoro el resto de la información */
	char *token;
	token = malloc(sizeof(char) * 256);
	char *delim = "-";
	token = strtok(buff, delim);
	token = strtok(NULL, delim);
	
	/* Elimino el espacio */
	token++;

	/* Dejo los 7 numeros de la patente */
	token[7] = '\0';

	fp = fopen(OUT, "w+");
	fwrite(token, sizeof(char), strlen(token), fp);
	fclose(fp);

	//printf("%s\n", token);

	// Llamo a la función de Assembler
	asm_main(1);
	
	return 0;
}