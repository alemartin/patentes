<?php include "../inc/dbinfo.inc"; ?>
<html>
<head>
  <title>Log - <?php echo htmlspecialchars($_GET["date"]); ?></title>
</head>
<body>

<?php

  /* Connect to MySQL and select the database. */
  $connection = mysqli_connect(DB_SERVER, DB_USERNAME, DB_PASSWORD);

  if (mysqli_connect_errno()) echo "Failed to connect to MySQL: " . mysqli_connect_error();

  $database = mysqli_select_db($connection, DB_DATABASE);

  /* Ensure that the EMPLOYEES table exists. */
  //VerifyEmployeesTable($connection, DB_DATABASE);

  /* If input fields are populated, add a row to the EMPLOYEES table. */
  /*$employee_name = htmlentities($_POST['NAME']);
  $employee_address = htmlentities($_POST['ADDRESS']);

  if (strlen($employee_name) || strlen($employee_address)) {
    AddEmployee($connection, $employee_name, $employee_address);
  }*/
?>


<p style="font-size:120%; background-color:#04151EB8; margin:0; margin-top:10; color:white; font-family:verdana; padding-top: 3px; padding-bottom: 3px"><b>
<span style="display:inline-block; width: 16%">Tiempo de Ingreso</span><span style="display:inline-block; width: 16%">Tiempo de Egreso</span><span style="display:inline-block; width: 16%">Tiempo de Estadía</span><span style="display:inline-block; width: 16%">Dominio</span><span style="display:inline-block; width: 16%">Nombre</span><span style="display:inline-block; width: 16%">Monto Abonado</span>
</b></p>

<?php

$result = mysqli_query($connection, "select ing.momento, egr.momento, estadia, patente, nombre, apellido, monto from ingreso_egreso i_e join ingresos ing on ing.id_ingreso = i_e.id_ingreso join egresos egr on egr.id_egreso = i_e.id_egreso
join autos on ing.auto  = autos.patente left outer join clientes on autos.id_propietario = clientes.id_cliente where date(ing.momento) = '".htmlspecialchars($_GET["date"])."'");

$odd = 1;

while($query_data = mysqli_fetch_row($result)) {

  if(($odd % 2) == 0){
    echo "<p style=\"background-color:#6881D88C; margin:0\">";
  }  else{
    echo "<p style=\"background-color:#0051ba99; margin:0\">";
  }

  // 
  echo "<span style=\"display:inline-block; width: 16%; margin-top: 5; margin-bottom: 5;\">",$query_data[0], "</span>";
  echo "<span style=\"display:inline-block; width: 16%; margin-top: 5; margin-bottom: 5;\">",$query_data[1], "</span>";
  echo "<span style=\"display:inline-block; width: 16%; margin-top: 5; margin-bottom: 5;\">",$query_data[2], "</span>";
  echo "<span style=\"display:inline-block; width: 16%; margin-top: 5; margin-bottom: 5;\">",$query_data[3], "</span>";
  echo "<span style=\"display:inline-block; width: 16%; margin-top: 5; margin-bottom: 5;\">",$query_data[4],", ", $query_data[5], "</span>";
  echo "<span style=\"display:inline-block; width: 16%; margin-top: 5; margin-bottom: 5;\">$ ",$query_data[6], "</span>";
  echo "</p>";

  $odd += 1;
}

?>

<!-- Clean up. -->
<?php

  mysqli_free_result($result);
  mysqli_close($connection);

?>

</body>
</html>


<?php

/* Add an employee to the table. */
/*function AddEmployee($connection, $name, $address) {
   $n = mysqli_real_escape_string($connection, $name);
   $a = mysqli_real_escape_string($connection, $address);

   $query = "INSERT INTO EMPLOYEES (NAME, ADDRESS) VALUES ('$n', '$a');";

   if(!mysqli_query($connection, $query)) echo("<p>Error adding employee data.</p>");
}*/

/* Check whether the table exists and, if not, create it. */
/*function VerifyEmployeesTable($connection, $dbName) {
  if(!TableExists("EMPLOYEES", $connection, $dbName))
  {
     $query = "CREATE TABLE EMPLOYEES (
         ID int(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
         NAME VARCHAR(45),
         ADDRESS VARCHAR(90)
       )";

     if(!mysqli_query($connection, $query)) echo("<p>Error creating table.</p>");
  }
}*/

/* Check for the existence of a table. */
/*function TableExists($tableName, $connection, $dbName) {
  $t = mysqli_real_escape_string($connection, $tableName);
  $d = mysqli_real_escape_string($connection, $dbName);

  $checktable = mysqli_query($connection,
      "SELECT TABLE_NAME FROM information_schema.TABLES WHERE TABLE_NAME = '$t' AND TABLE_SCHEMA = '$d'");

  if(mysqli_num_rows($checktable) > 0) return true;

  return false;
}*/
?>                        
                