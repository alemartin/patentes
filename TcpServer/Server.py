# Streaming Server

import socket
import subprocess
import binascii
import datetime
import mysql.connector
import time
import sys, getopt
import signal

#pip install mysql-connector
price_per_second = 0.017

SIG_PHOTO = 44

global conn
global mycursor
global mydb

# ------------------------ start function ------------------------ #

def receiveSignal(signalNumber, frame):

	# Ignoro a la señal hasta que se levante la barrera
	signal.signal(SIG_PHOTO, signal.SIG_IGN)
	return

def cleanUp(signalNumber, frame):
	# < Ctrl + C > => Cierro la conexión y termino
	global conn
	print('\nAdios!\n')
	conn.close()
	exit(0)

# ------------------------ end function ------------------------ #

# ------------------------ start function ------------------------ #

def checkPatente(plate):
	
	global mycursor
	global mydb

	# Chequeo si la patente existe en la base de datos
	#	de no ser así hago un insert
	val = (plate,)
	mycursor.execute('SELECT * FROM autos WHERE patente=%s', val)
	myresult = mycursor.fetchone()
	if(not myresult):
		mycursor.execute('INSERT INTO autos (patente) VALUES (%s)', val)
		mydb.commit()

	# Chequeo si el auto tiene un egreso
	#	con una fecha mayor a la del ultimo ingreso
	#	de ser asi se trata de un nuevo ingreso
	#	caso contrario es un egreso
	val = (plate,)
	mycursor.execute('SELECT momento from ingresos where auto=%s order by momento desc limit 1', val)
	myresult = mycursor.fetchone()
	if(myresult):

		val = (plate, myresult[0])
		mycursor.execute('SELECT id_egreso from egresos where auto=%s and momento >= %s ', val)
		myresult = mycursor.fetchone()
		if(not myresult):
			
			# inserto valor en egreso
			val = (plate,)
			mycursor.execute('INSERT INTO egresos (auto) VALUES (%s)', val)
			mydb.commit()
			print('Egreso: ', plate)

			# obtengo id_ingreso
			val = (plate,)
			mycursor.execute('SELECT id_ingreso from ingresos where auto = %s order by momento desc limit 1', val)
			id_ingreso = mycursor.fetchone()
			
			# obtengo id_egreso
			mycursor.execute('SELECT id_egreso from egresos where auto = %s order by momento desc limit 1', val)
			id_egreso = mycursor.fetchone()
			
			# obtengo tiempo de estadía
			val = (id_ingreso[0], id_egreso[0])
			mycursor.execute('SELECT timediff(momento, (select momento from ingresos where id_ingreso = %s)) from egresos where id_egreso = %s', val)
			estadia = mycursor.fetchone()
			
			# calculo monto a pagar
			monto = estadia[0].total_seconds() * price_per_second

			# inserto valores en ingreso_egreso
			val = (id_ingreso[0], id_egreso[0], estadia[0], monto)
			mycursor.execute('INSERT INTO ingreso_egreso (id_ingreso, id_egreso, estadia, monto) VALUES (%s, %s, %s, %s)', val)
			mydb.commit()

			return
		
	# inserto valor en ingreso
	val = (plate,)
	mycursor.execute('INSERT INTO ingresos (auto) VALUES (%s)', val)
	mydb.commit()
	print('Ingreso: ', plate)

# ------------------------ end function ------------------------ #

# ------------------------ start function ------------------------ #

def scriptImage():

	subprocess.call(['./generarImagen.sh'], shell=True)
	subprocess.call(['printf "\n\nalpr -c eu -n 1 Recibido/" > alpr.sh'], shell=True)
	subprocess.call(['ls Recibido -t -1 | head -1 >> alpr.sh'], shell=True)
	subprocess.call(['chmod +x alpr.sh'], shell=True)
	subprocess.call(['./alpr.sh > result.txt'], shell=True)
	subprocess.call(['rm alpr.sh'], shell=True)

# ------------------------ end function ------------------------ #


def main(argv):

	signal.signal(SIG_PHOTO, signal.SIG_IGN)

	# ............................................ #

	# Fijo la IP del Servidor mediante un argumento
	#	la llamada al programa debe ser de la siguiente manera
	#	Server.py -i <IP address>

	HOST = '' #ip del server
	PORT = 27418 #puerto del server
	SIZE = 2048 #tamaño del buffer
	NCLI = 1 #número máximo de conexiones

	# Parseo argumentos

	try:
		opts, args = getopt.getopt(argv,"i:",["host="])
	except getopt.GetoptError:
		print ('Server.py -i <IP>')
		sys.exit(2)
	if len(opts) != 1 or len(args) != 0 :
		print ('Server.py -i <IP>')
		sys.exit(2)
	for opt, arg in opts:
		if opt in ("-i", "--host"):
			HOST = arg

	# ............................................ #

	# Me conecto con la base de datos

	print('\nConectando con la base de datos...')

	global mycursor
	global mydb

	mydb = mysql.connector.connect(
	  host="parking-db.cikxs5avqneu.us-east-1.rds.amazonaws.com",
	  user="admin",
	  passwd="1123581321",
	  database="parkinglot"
	)

	mycursor = mydb.cursor()

	# ............................................ #

	# Me conecto con el celular

	print ('Esperando conexion con la camara...')

	#HOST = '192.168.0.188' #ip del server

	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	s.bind((HOST, PORT))
	s.listen(NCLI) # Espero al cliente

	global conn

	conn, addr = s.accept()
	print ('Se conectó un cliente', addr)

	# ............................................ #

	signal.signal(signal.SIGINT, cleanUp)

	# Loop principal

	while (1):
		
		data = conn.recv(SIZE) # Leo el socket

		# Si la lectura es waiting, el cliente me
		#	indica que espera la indicación para
		#	sacar la foto y enviarla
		if data==b'WAITING\n':

			# Defino handler de la señal
			signal.signal(SIG_PHOTO, receiveSignal)
			
			print('>> Esperando un vehículo')
			
			# Espero hasta que recibo la señal
			#	una vez recibida, mando orden
			#	al cliente de sacar la foto
			signal.pause()
			conn.send(b'OK\n')

		# De no ser así recibo la imagen
		else:
			start = time.time()
			# Abro archivo donde guardo la imagen codificada
			file = open("Image/imagenB64", "wb+")

			# Recibo el tamaño en bytes de la imagen
			# 	y los transformo en un entero
			msgLength = data.decode()
			bytesTotl = int(msgLength)
			bytesRead = 0

			# Envio ack del tamaño
			conn.send(b'OKK\n')

			# Leo y guardo los bytes por porciones
			#	cuando la cantidad de bytes leidos
			#	es mayor o igual a los bytes totales
			#	ya tengo mi imagen codificada en B64
			while (bytesRead < bytesTotl):
				data = conn.recv(SIZE)
				file.write(data)
				bytesRead = bytesRead+len(data)

			# Cierro archivo
			file.close()

			# Envío ack de la imagen
			conn.send(b'END\n')

			# Corro shell script para decodificar la imagen
			#	el resultado se guardará en 'result.txt'
			scriptImage()

			result = subprocess.call(['Parser/parser.out result.txt'], shell=True)
			if (result == 0) :
				file = open("parsed.txt", "r")
				resultado = file.read()
				print('Patente: ', resultado)
				checkPatente(resultado)
			else :
				print('No se encontro ninguna patente')

			end = time.time()
			print('It took ', end - start, ' seconds')


if __name__ == "__main__":
   main(sys.argv[1:])