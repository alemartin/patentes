#include <linux/module.h> /* Needed by all modules */
#include <linux/kernel.h> /* Needed for KERN_INFO */
#include <linux/init.h> /* Needed for the macros */
#include <linux/timer.h> 
#include <linux/gpio.h> /* Para el gpio */
#include <linux/delay.h>
#include <linux/jiffies.h>

#include <asm/siginfo.h>    //siginfo
#include <linux/rcupdate.h> //rcu_read_lock
#include <linux/sched.h>
#include <linux/sched/signal.h>

/* Parámetros */
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/stat.h>

MODULE_LICENSE("GPL");

#define SIG_TEST 44

#define LOW  0
#define HIGH 1

#define TRIG 13
#define ECHO 19

#define DIST 600
 
const int demora = 2000;
struct timer_list g_timer;
int settle = 1;
unsigned long beg, end, distance, duration;
int ret;

static int pid = -1; // Stores application PID in user space
struct siginfo info;
struct task_struct *tarea;

module_param(pid, int, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP);
MODULE_PARM_DESC(pid, "Server PID");


void gpio_init(void){

    printk(KERN_INFO "Starting gpio...");
    gpio_request(TRIG, "TRIG");
    gpio_direction_output(TRIG, LOW);
    gpio_direction_input(ECHO);
    printk(KERN_INFO "Starting gpio done.");

}

void gpio_exit(void){

    printk(KERN_INFO "Stopping gpio...");
    gpio_free(TRIG);
    gpio_free(ECHO);
    printk(KERN_INFO "Stopping gpio done.");

}

//void _TimerHandler(unsigned long data)
void _TimerHandler(struct timer_list  *timer)
{   

    if(settle == 1){
        settle+=1;
    }
    else{

        gpio_set_value(TRIG, HIGH);
        udelay(10);
        gpio_set_value(TRIG, LOW);

        while(gpio_get_value(ECHO) == 0){}

        udelay(DIST);

        ret = gpio_get_value(ECHO);

        if(ret != 1){
        //if(settle % 10 == 0){

            printk(KERN_INFO "Llego un auto!\n");

            if (tarea != NULL) {
                rcu_read_unlock();
                if (send_sig_info(SIG_TEST, &info, tarea) < 0){
                    printk(KERN_INFO "Error en el envio de la señal\n");
                    return;
                } // send signal
            }
            else {
                printk(KERN_INFO "Error: pid_task es nulo\n");
                rcu_read_unlock();
                return;
            }
        }

    }

    /*Reseteo timer para 2s*/
    mod_timer(&g_timer, jiffies + msecs_to_jiffies(demora));

    printk(KERN_INFO "Timer Handler\n");
}

/* Configura la señar de tiempo real
    SIGRTMIN+10 para avisar al programa
    principal de que llego un auto */

void set_sig(void) {

    memset(&info, 0, sizeof(struct siginfo));
    
    // Definimos numero de señal
    info.si_signo = SIG_TEST;
    
    // This is bit of a trickery: SI_QUEUE is normally used by sigqueue from user space,    and kernel space should use SI_KERNEL. 
    // But if SI_KERNEL is used the real_time data  is not delivered to the user space signal handler function. */
    info.si_code = SI_QUEUE;
    
    // SIGRTs pueden tener hasta 32 bits de data
    info.si_int = 1234;
    rcu_read_lock();

    // Buscamos la tarea con el pid del argumento
    tarea = pid_task(find_pid_ns(pid, &init_pid_ns), PIDTYPE_PID);

}
 
static int __init sensor_init(void) {

    printk(KERN_INFO "Sensor: Entrando en el kernel...\n");

    /* Si no se paso un pid como argumento, no hago nada */
    if(pid < 0){
        printk(KERN_INFO "Error: pid no valido\n");
        printk(KERN_INFO "Use: insmod <moudle.ko> pid=<value>\n");
    }
    else{
        
        printk(KERN_INFO "PID = %d\n", pid);

        /* Configuro GPIOs */
        gpio_init();

        /* Configuro señal */
        set_sig();

        /* Arranco timer */
        timer_setup(&g_timer, _TimerHandler, 0);
        mod_timer( &g_timer, jiffies + msecs_to_jiffies(demora));
    }
 
    return 0;
}
 
static void __exit sensor_exit(void) {
    
    if (pid > 0) {
        del_timer(&g_timer);
        gpio_exit();
    }

    printk(KERN_INFO "Sensor: Saliendo del kernel...\n");
}
 
module_init(sensor_init);
module_exit(sensor_exit);